**9352B-G6**

A repository containing three websites that promotes advocacies of a barangay,
specifically Rimando which is located at Baguio City.

---

## About The Project

This project is made in partial fulfillment of the course IT324L scheduled in
5:30PM-7:00PM WS under our instructor, Sir Roderick Makil.

Each website will promote an advocacy of the barangay in such a way that the 
three websites have the same style/design for which we will use two css files,
one for the professional looking and one for the funny type. 

---

## Members Of The Project

07, Dariano, Michael Jericho F.
08, Domaoa, Jeane Cris C.
27, Corilla, Patricia Adelle
29, Dayrit, Aika Vien F.
35, Natividad, Ernallyn
36, Paguio, Tanya Anjelica R.

---